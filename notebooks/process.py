import numpy as np 
import pandas as pd 


def import_csv(
    filename: str, 
    sep: str, 
    **kwargs
):

    print("Importing csv: ", filename)
    data = pd.read_csv(filename, sep=sep , error_bad_lines=False, index_col=False, dtype='unicode', encoding='latin1')
    return data

def trim(
    df, 
    order_date,
    order_quantity,
    order_value,
    category_code,
    material_id,
    currency,
    supplier_name,
    supplier_id,
    price_factor=None,
    article_description_1=None,
    article_description_2=None,
    **kwargs
):
    
    df = pd.DataFrame(df, columns= [order_date, order_quantity, order_value, category_code, material_id, currency,supplier_name, supplier_id, 
                                    price_factor, article_description_1, article_description_2])
    df = df.rename(columns = {order_date: "order_date", order_quantity: "order_quantity", order_value: "order_value", category_code: "category_code", material_id: "material_id",
                              currency: "currency", supplier_name : "supplier_name", supplier_id: "supplier_id", price_factor: "price_factor", 
                              article_description_1: "article_description_1", article_description_2: "article_description_2"})
    
    return df

def clean(
    df
):
    df['order_quantity'] = df['order_quantity'].astype(int)
    df['order_value'] = df['order_value'].str.replace(',','.').astype(float)
    
    df.drop(df[df['order_value'] <= 0].index, inplace = True)

    # df['category_code'] = df['category_code'].astype(str)
    # df['material_id'] = df['material_id'].astype(str)
    # df['currency'] = df['currency'].astype(str)
    # df['supplier_name'] = df['supplier_name'].astype(str)
    # df['supplier_id'] = df['supplier_id'].astype(str)
    # df['price_factor'] = df['price_factor'].astype(str) 
    # df['article_description_1'] = df['article_description_1'].astype(str)
    # df['article_description_2'] = df['article_description_2'].astype(str)
    
    return df
    
def generate_deviations(
    df
):

    df['unit_price'] = df['order_value']/df['order_quantity']
    df['deviation'] = '0.0'
    df['deviation'] = df['deviation'].astype(float)
    df['impact_value'] = '0.0'
    df['impact_value'] = df['impact_value'].astype(float)
    
    df_grouped = df.sort_values(['order_date'],ascending=False).groupby(['material_id', 'supplier_id'])

    for name, group in df_grouped:
        
        deviation = 0
        impact_value = 0
        unit_price_smallest = float('inf')
        
        # Find the smallest unit price in each group

        for row_index, row in group.iterrows():
            if ((row['unit_price'] != 0) and (row['unit_price'] < unit_price_smallest)):
                unit_price_smallest = row['unit_price']
        
        # Calculate deviation from the smallest unit price for each row
        for row_index, row in group.iterrows():
            group.loc[row_index,'deviation'] = ((row['unit_price'] - unit_price_smallest)/ unit_price_smallest) * 100
            group.loc[row_index, 'impact_value'] = row['order_value'] - (row['order_quantity'] * unit_price_smallest)   
    
    return df